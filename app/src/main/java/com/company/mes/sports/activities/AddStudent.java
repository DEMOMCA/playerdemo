package com.company.mes.sports.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.models.Student;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class AddStudent extends AppCompatActivity {

    EditText txtId, txtName, txtDob, txtStandard, txtGuardian, txtAddress, txtPhone, txtBlood;
    RadioButton rdoMale;

    String TAG = "AddStudent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        getSupportActionBar().setTitle("Add Student");

        rdoMale = (RadioButton)findViewById(R.id.male);
        txtId = (EditText) findViewById(R.id.admno);
        txtName = (EditText) findViewById(R.id.name);
        txtDob = (EditText) findViewById(R.id.dob);
        txtStandard = (EditText) findViewById(R.id.standard);
        txtGuardian = (EditText) findViewById(R.id.guardian);
        txtAddress = (EditText) findViewById(R.id.address);
        txtPhone = (EditText) findViewById(R.id.phone);
        txtBlood = (EditText) findViewById(R.id.blood);


    }

    public void createStudent(View view) {
        if (isValid()){
            Student student = new Student();
            student.setId(txtId.getText().toString())
                    .setName(txtName.getText().toString())
                    .setGender(rdoMale.isChecked() ? "Male" : "Female")
                    .setDob(txtDob.getText().toString())
                    .setStandard(txtStandard.getText().toString())
                    .setGuardian(txtGuardian.getText().toString())
                    .setAddress(txtAddress.getText().toString())
                    .setPhone(txtPhone.getText().toString())
                    .setBlood(txtBlood.getText().toString());
            Log.d(TAG, "createStudent: "+student.toString());
            MainActivity.databaseReference.child("students").child(txtId.getText().toString())
                    .setValue(student)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(AddStudent.this,R.string.successfully_created, Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure: createStudent", e);
                    Toast.makeText(AddStudent.this,R.string.oops, Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    private boolean isValid() {
        return true;
    }
}
