package com.company.mes.sports.models;

public class Schedule {
    String id, timestamp, sport, attendance_status = "Scheduled";

    public String getTimestamp() {
        return timestamp;
    }

    public Schedule setTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return Schedule.this;
    }

    public String getSport() {
        return sport;
    }

    public Schedule setSport(String sport) {
        this.sport = sport;
        return Schedule.this;
    }

    public String getId() {
        return id;
    }

    public Schedule setId(String id) {
        this.id = id;
        return Schedule.this;
    }

    public String getAttendance_status() {
        return attendance_status;
    }

    public Schedule setAttendance_status(String attendance_status) {
        this.attendance_status = attendance_status;
        return Schedule.this;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id='" + id + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", sport='" + sport + '\'' +
                ", attendance_status='" + attendance_status + '\'' +
                '}';
    }
}
