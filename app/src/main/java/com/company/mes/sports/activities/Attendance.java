package com.company.mes.sports.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.adapters.AttendanceAdapter;
import com.company.mes.sports.models.AttendanceStatus;
import com.company.mes.sports.models.Student;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;

public class Attendance extends AppCompatActivity {

    private String id, TAG = "ATTENDANCE_ACTIVITY";
    int index = 0;
    Student student;
    List<AttendanceStatus> attendanceStatusList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AttendanceAdapter attendanceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Attendance");

        id = getIntent().getStringExtra("id");

        attendanceAdapter = new AttendanceAdapter(attendanceStatusList, Attendance.this);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(Attendance.this);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(attendanceAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attendanceStatusList.clear();
                alert();
            }
        });

        ChildEventListener attendanceStatusEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                AttendanceStatus attendanceStatus = dataSnapshot.getValue(AttendanceStatus.class);
                attendanceStatusList.add(attendanceStatus);
                attendanceAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        MainActivity.databaseReference.child("schedule").child(id).child("attendance").addChildEventListener(attendanceStatusEventListener);
    }

    private void alert() {

        //Last student
        if (!(index < MainActivity.studentList.size())){
            if (index != 0)
                store();
            return;
        }

        student = MainActivity.studentList.get(index++);
        new AlertDialog.Builder(Attendance.this)
                .setTitle(student.getName())
                .setMessage("Admission No: "+student.getId())
                .setPositiveButton(R.string.present, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        addAttendance(R.string.present);
                    }
                }).setNegativeButton(R.string.absent, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                addAttendance(R.string.absent);
            }
        }).show();
    }

    private void store() {
        MainActivity.databaseReference.child("schedule").child(id).child("attendance").setValue(attendanceStatusList)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        toast(R.string.successfully_created);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "onFailure: ", e);
                toast(R.string.oops);
            }
        });
        attendanceAdapter.notifyDataSetChanged();
    }

    private void addAttendance(int status) {
        AttendanceStatus attendanceStatus = new AttendanceStatus();
        attendanceStatus.setId(student.getId());
        attendanceStatus.setName(student.getName());
        attendanceStatus.setStatus(getResources().getString(status));
        attendanceStatusList.add(attendanceStatus);
        alert();
    }

    private void toast(int message) {
        Toast.makeText(Attendance.this, message, Toast.LENGTH_LONG).show();
    }

}
