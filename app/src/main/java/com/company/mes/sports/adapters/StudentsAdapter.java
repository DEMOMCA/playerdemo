package com.company.mes.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.mes.sports.R;
import com.company.mes.sports.activities.HealthHistory;
import com.company.mes.sports.models.Student;

import java.util.List;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.MyViewHolder> {
    private List<Student> studentList;
    private Context context;

    public StudentsAdapter(List<Student> studentList, Context context) {
        this.studentList = studentList;
        this.context = context;
    }

    @NonNull
    @Override
    public StudentsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.students_list_row, viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentsAdapter.MyViewHolder myViewHolder, int i) {
        final Student student = studentList.get(i);
        myViewHolder.txtName.setText(student.getId()+" - "+student.getName());
        myViewHolder.txtPhone.setText(student.getPhone());
        myViewHolder.txtAddress.setText(student.getAddress());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HealthHistory.class);
                intent.putExtra("id", student.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView txtName, txtPhone, txtAddress;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView)itemView.findViewById(R.id.name);
            txtPhone = (TextView)itemView.findViewById(R.id.phone);
            txtAddress = (TextView)itemView.findViewById(R.id.address);
        }
    }
}
