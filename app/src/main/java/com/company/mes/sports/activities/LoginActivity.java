package com.company.mes.sports.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.company.mes.sports.R;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    public static EditText edit_username, edit_password;
    public SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("Login");

        edit_username = (EditText)findViewById(R.id.username);
        edit_password = (EditText)findViewById(R.id.password);

        sharedPreferences = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        if (sharedPreferences.contains("username")){
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }
    }
    public void forgot(View view) {
        startActivity(new Intent(this, ResetPasswordActivity.class));
    }

    public void login(View view) {
        if (isVerified()){
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }
    }

    private boolean isVerified() {
        if (!sharedPreferences.getString("username","null").equals(edit_username.getText().toString())){
            Toast.makeText(LoginActivity.this, "Invalid Username", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!edit_password.getText().toString().equals(sharedPreferences.getString("password","null"))){
            Toast.makeText(LoginActivity.this, "Invalid password", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void signUp(View view) {
        startActivity(new Intent(LoginActivity.this, SignupActivity.class));
        finish();
    }
}
