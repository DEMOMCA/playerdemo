package com.company.mes.sports.models;

public class Sport {
    String id, name, type;

    public String getName() {
        return name;
    }

    public Sport setName(String name) {
        this.name = name;
        return Sport.this;
    }

    public String getType() {
        return type;
    }

    public Sport setType(String type) {
        this.type = type;
        return Sport.this;
    }

    public String getId() {
        return id;
    }

    public Sport setId(String id) {
        this.id = id;
        return Sport.this;
    }

    @Override
    public String toString() {
        return "Sport{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
