package com.company.mes.sports.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.mes.sports.R;
import com.company.mes.sports.activities.Attendance;
import com.company.mes.sports.models.Schedule;

import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.MyViewHolder> {
    private Context context;
    private List<Schedule> scheduleList;

    public ScheduleAdapter(Context context, List<Schedule> scheduleList) {
        this.context = context;
        this.scheduleList = scheduleList;
    }

    @NonNull
    @Override
    public ScheduleAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.schedule_list_row,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleAdapter.MyViewHolder myViewHolder, int i) {
        final Schedule schedule = scheduleList.get(i);
        myViewHolder.txtTimestamp.setText(schedule.getTimestamp());
        myViewHolder.txtSport.setText(schedule.getSport());
        myViewHolder.txtAttendanceStatus.setText(schedule.getAttendance_status());
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Attendance.class);
                intent.putExtra("id", schedule.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return scheduleList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtTimestamp, txtSport, txtAttendanceStatus;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTimestamp = (TextView)itemView.findViewById(R.id.timestamp);
            txtSport = (TextView)itemView.findViewById(R.id.sport);
            txtAttendanceStatus = (TextView)itemView.findViewById(R.id.attendance_status);
        }
    }
}
